import boto3
import pytest
from botocore.exceptions import ParamValidationError
from moto import mock_dynamodb2
from src.boto3_example import DynamodBExample
from botocore.exceptions import ClientError
DynamodBExample_instance = DynamodBExample()

@mock_dynamodb2
def test_create_dynamo_table():
    actual = None

    dynamodb = boto3.resource('dynamodb', region_name='us-east-1')

    table = DynamodBExample_instance.create_movies_table(dynamodb)

    assert table.name == 'Movies'


@mock_dynamodb2
def test_add_dynamo_record_table():
    dynamodb = boto3.resource('dynamodb', region_name='us-east-1')

    table = DynamodBExample_instance.create_movies_table(dynamodb)

    result = DynamodBExample_instance.add_dynamo_record("Movies","The Big New Movie", 2015,"Nothing happens at all.", 0 , dynamodb)

    assert result['ResponseMetadata']['HTTPStatusCode'] == 200

@mock_dynamodb2
def test_add_dynamo_record_table_failure():
    dynamodb = boto3.resource('dynamodb', region_name='us-east-1')

    table = DynamodBExample_instance.create_movies_table(dynamodb)
    try:
        with pytest.raises(ParamValidationError):
            # We need to create the bucket since this is all in Moto's 'virtual' AWS account
            table = DynamodBExample_instance.create_movies_table(dynamodb)
            DynamodBExample_instance.add_dynamo_record("Movies/", "The Big New Movie", 201,
                                                       "Nothing happens at all.", 0,
                                                       dynamodb)
    except:
        print("Invalid parameter")

