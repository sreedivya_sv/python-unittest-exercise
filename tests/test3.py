from pprint import pprint
import unittest
import boto3  # AWS SDK for Python
from botocore.exceptions import ClientError
from moto import mock_dynamodb2  # since we're going to mock DynamoDB service


@mock_dynamodb2
class TestDatabaseFunctions(unittest.TestCase):

    def setUp(self):
        """
        Create database resource and mock table
        """
        self.dynamodb = boto3.resource('dynamodb', region_name='us-east-1')

        from src.boto3_example import DynamodBExample
        self.table = DynamodBExample(self.dynamodb)


if __name__ == '__main__':
    unittest.main()