class TDDExample():
    def __init__(self):
        pass

    def reverse_string(self, input_str: str) -> str:
        return input_str[::-1]

    def find_longest_word(self, sentence: str) -> str:
        longest = max(sentence.split(), key=len)

        return longest

    def reverse_list(self, input_list: list) -> list:
        x = input_list[::-1]
        return x

    def count_digits(self, input_list: list, number_to_be_counted: int) -> int:
        c = 0
        for i in input_list:
            if i == number_to_be_counted:
                c = c + 1
        return c